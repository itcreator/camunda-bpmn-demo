use std::rc::Rc;
use std::sync::Arc;
use camunda_client::apis::ExternalTaskApiClient;
use crate::external_task_handler::ExternalTaskHandler;

use config::create_camunda_config;

pub struct DiC {
    // Public services
    pub external_task_handler: Arc<ExternalTaskHandler>,
}

impl DiC {
    pub fn new(
        external_task_handler: Arc<ExternalTaskHandler>,
    ) -> DiC {

        DiC {
            external_task_handler,
        }
    }
}

pub fn di_factory() -> DiC {
    //Default Camunda configuration 12
    let camunda_config = Rc::new(create_camunda_config());

    let camunda_external_task_client = Arc::new(ExternalTaskApiClient::new(camunda_config.clone()));
    let external_task_handler = Arc::new(ExternalTaskHandler::new(camunda_external_task_client));


    DiC::new(
        external_task_handler,
    )
}
