use core::time;
use std::sync::Arc;
use std::thread::sleep;
use camunda_client::apis::{Error, ExternalTaskApi, ExternalTaskApiClient};
use camunda_client::models::{CompleteExternalTaskDto, FetchExternalTasksDto, FetchExternalTaskTopicDto, LockedExternalTaskDto};
use log::{info, warn};


pub struct ExternalTaskHandler {
    camunda_external_task_client: Arc<ExternalTaskApiClient>,
}

impl ExternalTaskHandler {
    pub fn new(camunda_external_task_client: Arc<ExternalTaskApiClient>) -> ExternalTaskHandler {
        ExternalTaskHandler {
            camunda_external_task_client,
        }
    }

    fn fetch_item (&self) -> Result<Vec<LockedExternalTaskDto>, Error> {
        // TODO: move topic name to config
        let topic_name = "extdemo";
        // TODO: configure supported versions;
        //TODO: move worker_id to config
        let worker_id = "demo-worker";

        let res = self.camunda_external_task_client
            .fetch_and_lock(Option::from(FetchExternalTasksDto {
                worker_id: worker_id.to_string(),
                max_tasks: Some(1),
                use_priority: None,
                async_response_timeout: None,
                topics: Some(vec![FetchExternalTaskTopicDto::new(topic_name.to_string(), Some(60))]),
            }));

        res
    }

    pub fn handle(&self) {
        //TODO: move worker_id to config
        let worker_id = "demo-worker";

        loop {
            let res = self.fetch_item();

            match res {
                Ok(tasks_dto) => {
                    info!("Data for external task fetched successfully.\n{:?}", tasks_dto);
                    for task_dto in tasks_dto {
                        self.camunda_external_task_client
                            .complete_external_task_resource(
                                &task_dto.id.clone().unwrap(),
                                Some(CompleteExternalTaskDto { worker_id: Some(worker_id.to_string()), variables: None, local_variables: None })
                            ).expect("Can't complete an external task");
                    }
                },
                Err(e) => {
                    warn!("Can't fetch data for external task. Error: {:?}", e);
                }
            };

            sleep(time::Duration::from_secs(1));
        }
    }
}
