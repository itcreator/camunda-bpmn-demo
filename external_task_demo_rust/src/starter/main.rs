extern crate core;

use crate::di::di_factory;

pub mod process_starter;
mod di;

fn main() -> Result<(), String> {
    env_logger::init();

    let di = di_factory();

    di.process_starter.start_process()
}
