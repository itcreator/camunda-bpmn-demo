use std::sync::Arc;
use camunda_client::apis::ProcessDefinitionApi;
use camunda_client::apis::ProcessDefinitionApiClient;
use camunda_client::models::StartProcessInstanceDto;
use log::{info, error};

pub struct ProcessStarter {
    camunda_process_definition_client: Arc<ProcessDefinitionApiClient>,
}

impl ProcessStarter {
    pub fn new(camunda_process_definition_client: Arc<ProcessDefinitionApiClient>) -> ProcessStarter {
        ProcessStarter {
            camunda_process_definition_client,
        }
    }

    pub fn start_process(&self) -> Result<(), String> {
        let process_definition_id = "Process_demo:1:82236afc-ace9-11ed-b3a6-0242ac120002";

        let res = self.camunda_process_definition_client.start_process_instance(process_definition_id, Some(StartProcessInstanceDto::new()));
        match res {
            Ok(instance_dto) => {
                info!("Process started{:?}", instance_dto);
                Ok(())
            },
            Err(e) => {
                error!("Some error {:?}", e);
                Err("Some error".to_owned())
            }
        }
    }
}
