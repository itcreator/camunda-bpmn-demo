use std::rc::Rc;
use std::sync::Arc;
use camunda_client::apis::ProcessDefinitionApiClient;
use crate::process_starter::ProcessStarter;

use config::create_camunda_config;

pub struct DiC {
    // Public services
    pub process_starter: Arc<ProcessStarter>,
}

impl DiC {
    pub fn new(
        process_starter: Arc<ProcessStarter>,
    ) -> DiC {

        DiC {
            process_starter,
        }
    }
}

pub fn di_factory() -> DiC {
    //Default Camunda configuration
    let camunda_config = Rc::new(create_camunda_config());

    let camunda_process_definition_client = Arc::new(ProcessDefinitionApiClient::new(camunda_config.clone()));
    let process_starter = Arc::new(ProcessStarter::new(camunda_process_definition_client));

    DiC::new(
        process_starter,
    )
}
